/*
  taski:
  gulp scss - Kompilacja plików scss
  gulp js - Minifikacja plików js i łączenie w jeden plik
  gulp sprites - Tworzenie pliku sprites
    w głównym scss dodać:
    @import './_sprites.scss';
    i w użycie w selektorze
    @include sprite($arrow-down);
  gulp spritesRetina - Tworzenie pliku sprites pod retina (nie działa - https://www.npmjs.com/package/gulp.spritesmith.3x)
  gulp htmlMin - Minifikacja plików html
  gulp images - Minifikacja plików graficznych
  gulp watch (default) - Obserwowanie zmian w plikach sccs i js
  gulp compile - Kompilacja scss/js bez watcha
*/

const gulp = require("gulp");
const sass = require("gulp-sass"); // Sass engine
const cssnano = require("gulp-cssnano"); // uglifikacja CSS, autoprefixer
const uglify = require("gulp-minify");
const concat = require("gulp-concat"); // łączenie plików
const pump = require("pump");
const spritesmith = require('gulp.spritesmith'); // tworzenie spritów
const sprite = require('gulp.spritesmith.3x');
const gulpif = require('gulp-if'); // dodane warunki w taskach
const htmlmin = require('gulp-htmlmin'); // kompresja HTML
const imagemin = require('gulp-imagemin'); // kompresja obrazkow

// opcja sass
let sassOptions = {
  errLogToConsole: true,
  outputStyle: 'compressed',
  includePaths: "node_modules/normalize-scss/sass/"
};

// ścieżki do plików
let scssSources = [
  "./node_modules/bootstrap/scss/bootstrap.scss",
  // "./node_modules/slick-carousel/slick/slick.scss",
  "./src/scss/main.scss"
];
let cssFile = "main.css";  // nazwa pliku CSS
var cssTargetPath = 'dist/css'; // ścieżka do pliku CSS

let jsSources = [
  "./node_modules/jquery/dist/jquery.min.js",
  "./node_modules/bootstrap/dist/js/bootstrap.min.js",
  // "./node_modules/slick-carousel/slick/slick.js",
  "./src/js/main.js"
];
var jsTargetPath = 'dist/js'; // ścieżka do pliku JS
var jsFile = 'main.js'; // nazwa pliku JS

var imgSourcePath = './src/img'; // ścieżka do plików źródłowych obrazków
var imgTargetPath = './img'; // ścieżka do plików docelowych obrazków

var spriteSourcePath = './src/img/sprites'; // ścieżka do plików źródłowych obrazków
var spriteFile = 'sprite.png'; // nazwa pliku sprites

// Kompilacja plików sass na css
gulp.task("scss", function () {
  return pump([
    gulp.src(scssSources),
    sass(sassOptions).on('error', sass.logError),
    concat(cssFile),
    cssnano({ zindex: false }),
    gulp.dest(cssTargetPath)
  ]);
});

// Minifikacja plików js i łączenie w jeden plik
gulp.task("js", function () {
  return pump([
    gulp.src(jsSources),
    concat(jsFile),
    uglify({ ext: { min: '.min.js' }, }),
    gulp.dest(jsTargetPath)
  ]);
});

// Obserwowanie zmian w plikach sccs i js
gulp.task('watch', function () {
  gulp.watch('./src/scss/**/*.scss', ['scss'])
    .on('change', function (event) {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
  gulp.watch('./src/js/**/*.js', ['js'])
    .on('change', function (event) {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

// Kompilacja bez watcha
gulp.task("compile", ["scss", "js"]);

// Tworzenie pliku sprites
gulp.task('sprites', function() {
  var spriteData =
    gulp.src(spriteSourcePath + '/*.*')
      .pipe(spritesmith({
        imgName: spriteFile,
        cssName: '_sprites.scss',
        imgPath: '../../img/' + spriteFile,
        cssFormat: 'scss',
        padding:1,
        cssVarMap: function(sprite) {
            sprite.name = sprite.name;
        }
      }));
  spriteData.img.pipe(gulp.dest('./img/'));
  spriteData.css.pipe(gulp.dest('./src/scss/'));
});

gulp.task('spritesRetina', function() {
  var spriteData = gulp.src(spriteSourcePath + '/*.*')
    .pipe(sprite({
      retinaSrcFilter: spriteSourcePath + '/*@2x.png',
      retinaImgName: 'sprite@2x.png',
      retina3xSrcFilter: spriteSourcePath +  '/*@3x.png',
      retina3xImgName: 'sprite@3x.png',
      imgName: 'sprite.png',
      imgPath: '../../img/sprite.png',
      retinaImgPath: '../../img/sprite@2x.png',
      retina3xImgPath: '../../img/sprite@3x.png',
      cssName: 'sprites.css'
    }));
 
  spriteData.img.pipe(gulp.dest('./dist/img/'));
  spriteData.css.pipe(gulp.dest('./sass/'));
});

// Minifikacja plików html
gulp.task('htmlMin', function() {
  return gulp.src('./**/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist'));
});

// Minifikacja plików graficznych
gulp.task('images', function() {
  return gulp.src(imgSourcePath + '/*.+(png|jpg|gif|svg)')
  .pipe(imagemin())
  .pipe(gulp.dest(imgTargetPath));
});

// domyślnie uruchamiany task
gulp.task('default', ['watch']);